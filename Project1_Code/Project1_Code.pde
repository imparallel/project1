/*
	Project 1
	Name of Project: ABCDE!! (Avoid Bullets & Crash Da Enemy !!)
	Author: 20150622 Changbok Lee
	Date: 2020.05
*/

import ddf.minim.*;

Step s =new Step();
Minim minim;
AudioPlayer intro;
AudioPlayer bgm;
AudioPlayer loadingm;
AudioPlayer crash;
AudioPlayer crashed;
AudioPlayer gameoverm;
AudioInput input;
PImage start;
PImage bg;
PImage loading;
PImage GameOver;
int now_time = millis();
float time = 0;
ArrayList<Enemy> enemies;
int madeNormalEnemy = 0;
ArrayList<PVector> eDir;
EnemyFactory ef;
int nesize = 30;
int besize = 20;
int bsize = 30;
ArrayList<Bullet> bullets;
ArrayList<PVector> bDir;
int crashs;
//int ingametime;
PFont font;
Char playa;
int psize = 24;

class Step
{
  GameState state;
  
  void startgame()
  {
    setState (new Title());
  }
  
  void setState (GameState s)
  {
    state= s;
  }
  
  void NextStep()
  {
    state.nextStep(this);
  }
}

interface GameState
{
  void nextStep (Step step);
}

class Title implements GameState
{
  void nextStep (Step step)
  {
    step.setState (new Loading());
  }
}

class Loading implements GameState
{
  void nextStep (Step step)
  {
    step.setState (new Ingame());
  }
}

class Ingame implements GameState
{
  void nextStep (Step step)
  {
    step.setState (new Gameover());
  }
}

class Gameover implements GameState
{
  void nextStep (Step step)
  {
    step.setState (new Loading());
  }
}


void setup()
{
	// your code
  // println ("hello world");  
  size(800,600);
  smooth();
  minim = new Minim(this);
  intro = minim.loadFile("intro.mp3");
  bgm = minim.loadFile("bgm.mp3");
  loadingm = minim.loadFile("loading.mp3");
  crash = minim.loadFile("crash.mp3");
  crashed = minim.loadFile("crashed.mp3");
  gameoverm = minim.loadFile("gameover.mp3");
  start = loadImage("start.png");
  bg = loadImage("bg.png");
  loading = loadImage("loading.png");
  GameOver = loadImage ("GameOver.png");
  font = loadFont("font.vlw");
  playa = new Char();
  enemies = new ArrayList<Enemy>(); 
  eDir = new ArrayList<PVector>();
  ef = new EnemyFactory();
  bullets = new ArrayList<Bullet>();
  bDir = new ArrayList<PVector>();
  crashs = 0;
  time = 0;
  s.startgame();
  intro.setGain(-15);
  bgm.setGain(-15);
  loadingm.setGain(-15);
  crash.setGain(-15);
  crashed.setGain(-25);
  gameoverm.setGain(-20);
  intro.loop();
  loadingm.play();
  //frameRate(60);
}

void draw()
{
	// your code
  //println (s.state);
  if (s.state instanceof Ingame)
  {
    //tint(255);
    image(bg,0,0,800,600);
    time = (millis()-now_time)/1000f;
    fill(110);
    textAlign(CENTER);
    text(time, width/2, 50);
    if (crashs == 1)
    {
      text("YOU CRASHED  " + crashs + "  ENEMY !!", width/2, 580);
    }
    else text("YOU CRASHED  " + crashs + "  ENEMIES !!", width/2, 580);
    playa.draw();
    int enemyHittedIndex = -1;
    int bulletOutIndex = -1;
    
    if (frameCount % 50 == 0)
    {
      makeEnemy();
    }
    
    for (Enemy enemy : enemies)
    {
      enemy.draw();
      enemy.go(eDir.get(enemies.indexOf(enemy)));
      if (enemy.x-enemy.w/2 < 0 || enemy.x+enemy.w/2 >800) 
      {
        eDir.get(enemies.indexOf(enemy)).x=-eDir.get(enemies.indexOf(enemy)).x;
      }
      if (enemy.y-enemy.h/2 < 0 || enemy.y+enemy.h/2 >600) 
      {
        eDir.get(enemies.indexOf(enemy)).y=-eDir.get(enemies.indexOf(enemy)).y;
      }
      if (frameCount % 20 == 0)
      {
      enemy.shoot();
      }
      if (enemy.hit(playa))
      {
        enemyHittedIndex = enemies.indexOf(enemy);
        crashs+=1;
        crash.rewind();
        crash.play();
      }
      if (enemy.barrierhit(playa))
      {
        enemy.barrier = false;
        crash.rewind();
        crash.play();
      }
      
    }
    
    if(enemyHittedIndex > -1) 
    {
      enemies.remove(enemyHittedIndex);
      eDir.remove(enemyHittedIndex);
    }
    
    for (Bullet bullet : bullets)
    {
      bullet.draw();
      bullet.go(bDir.get(bullets.indexOf(bullet)));
      if (bullet.loc.x < 0 || bullet.loc.x > 800 || bullet.loc.y < 0 || bullet.loc.y > 600)
      {
        bulletOutIndex = bullets.indexOf(bullet);
      }
      if (bullet.hit(playa))
      {
        crashed.rewind();
        crashed.play();
        gameoverm.rewind();
        gameoverm.loop();
        s.NextStep();
      }
    }
    
    if (bulletOutIndex > -1)
    {
      bullets.remove(bulletOutIndex);
      bDir.remove(bulletOutIndex);
    }
  }

  else if (s.state instanceof Title)
  {
    //tint(255);
    image(start,0,0,800,600);
    if (frameCount % 32 < 16)
    {
      fill(110);
      textFont(font,16);
      textAlign(CENTER);
      text("click anywhere to start", width/2, height/2+150);
    }
    else
    {
    }
    now_time = millis();
    startgame();
  }
  
  else if (s.state instanceof Loading)
  {
   //tint(255);
   intro.pause();
   gameoverm.pause();
   image(loading,0,0,800,600);
   enemies.removeAll(enemies);
   bullets.removeAll(bullets);
   madeNormalEnemy = 0;
   crashs = 0 ;
   int new_time = millis()-now_time;
   if(new_time > 1000)
   {
     now_time = millis();
     s.NextStep();
   }
  }
  
  else if (s.state instanceof Gameover)
  {
    //tint(255,30);
    bgm.pause();
    image(GameOver,0,0,800,600);
    fill(50);
    textAlign(CENTER);
    text("YOU AVOIDED BULLETS FOR " + int(time) + " SECS", width/2, 225);
    if (crashs == 1)
    {
      text("YOU CRASHED  " + crashs + "  ENEMY", width/2, 335);
    }
    else text("YOU CRASHED  " + crashs + "  ENEMIES", width/2, 335);
    if (frameCount % 32 < 16)
    {
      fill(110);
      textFont(font,16);
      textAlign(CENTER);
      text("click anywhere to restart", width/2, height/2+150);
    }
    else
    {
    }
    now_time = millis();
    startgame();
  }
  
  
}

void startgame()
{  
    if (mousePressed)
    {
      bgm.rewind();
      bgm.loop(255);
      s.NextStep();
    } 
}

void makeEnemy()
{
    enemies.add(ef.getNormalEnemy(random(nesize/2,width-nesize/2),random(nesize/2,height-nesize/2),nesize,nesize,false));
    if (madeNormalEnemy % 5 == 0)
    {
      enemies.add(ef.getBossEnemy(random((besize)/2,width-(besize)/2),random((besize)/2,height-(besize)/2),besize,besize,true));
    }
}

class Char //Player.
{
  float x = width/2;
  float y = height/2;
  float easing = 0.5;
  float targetX = mouseX;
  float targetY = mouseY;
  
  void draw()
  {
    x = x + (mouseX - x) * easing;
    y = y + (mouseY - y) * easing;
    fill(0,100);
    noStroke();
    ellipseMode(CENTER);
    ellipse(x, y, psize, psize);
  }
}

class EnemyFactory
{
  
  Enemy getNormalEnemy(float x,float y, float w,float h, boolean barrier)
  {
    NormalEnemy ne;
    ne = new NormalEnemy(x,y,w,h,barrier);
    eDir.add(new PVector(random(-1,1),random(-1,1)));
    madeNormalEnemy = madeNormalEnemy+1;
    return ne;
  }
  Enemy getBossEnemy(float x,float y, float w,float h, boolean barrier)
  {
    BossEnemy be;
    be = new BossEnemy(x,y,w,h,barrier);
    eDir.add(new PVector(random(-1,1),random(-1,1)));
    return be;
  }
}

    

abstract class Enemy
{
  float x;
  float y;
  float h;
  float w;
  boolean barrier;
  
  Enemy (float x, float y, float h, float w, boolean barrier)
  {
    this.x=x;
    this.y=y;
    this.h=h;
    this.w=w;
    this.barrier=barrier;
  }
  
  abstract void draw();
  abstract void shoot();
  abstract void go(PVector dir);
  abstract boolean barrierhit (Char playa);
  abstract boolean hit(Char playa);
  
}

class NormalEnemy extends Enemy
{
  
  NormalEnemy(float x,float y,float w,float h, boolean barrier)
  {
    super (x,y,w,h,barrier);
  }
  
  void draw()
  {
    rectMode(CENTER);
    noStroke();
    fill(50);
    rect(x,y,w,h);
  }
  
  void shoot()
  {
    float bulletX=x;
    float bulletY=y;
    bullets.add(new NormalBullet(bulletX,bulletY,8));
    bDir.add(new PVector(random(-1,1),random(-1,1)));
  }
  
  void go(PVector dir)
  {
        x+=dir.x*4;
        y+=dir.y*4;  
  }
  
  boolean barrierhit (Char playa)
  {
    return false;
  }
  
  boolean hit (Char playa)
  {
    if(playa.x + psize/2 < x-w/2) return false;
    else if(playa.y + psize/2 < y-h/2) return false;
    else if(playa.x - psize/2 > x+w/2) return false;
    else if(playa.y - psize/2 > y+h/2) return false;
    else return true;
  }
  
}

class BossEnemy extends Enemy
{
  
  BossEnemy(float x,float y,float w,float h, boolean barrier)
  {
    super (x,y,w,h,barrier);
  }
  
  void draw()
  {
    rectMode(CENTER);
    noStroke();
    fill(150,100);
    if (barrier == true)
    {
      rect(x,y,w+bsize,h+bsize);
    }
    fill(50);
    rect(x,y,w,h);
  }
  
  void shoot()
  {
    float bulletX=x;
    float bulletY=y;
    bullets.add(new NormalBullet(bulletX,bulletY,8));
    bDir.add(new PVector(1,1));
    bullets.add(new NormalBullet(bulletX,bulletY,8));
    bDir.add(new PVector(-1,1));
    bullets.add(new NormalBullet(bulletX,bulletY,8));
    bDir.add(new PVector(1,-1));
    bullets.add(new NormalBullet(bulletX,bulletY,8));
    bDir.add(new PVector(-1,-1));
    if (frameCount % 100 == 0)
    {
      bullets.add(new GuideBullet(bulletX,bulletY,12));
      bDir.add(new PVector((mouseX-bulletY)/100,(mouseY-bulletY)/100));
    }

  }
  
  void go(PVector dir)
  {
        x+=dir.x*4;
        y+=dir.y*4;  
  }
  
  boolean barrierhit (Char playa)
  {
    if(playa.x + psize/2 < x-(w+bsize)/2) return false;
    else if(playa.y + psize/2 < y-(h+bsize)/2) return false;
    else if(playa.x - psize/2 > x+(w+bsize)/2) return false;
    else if(playa.y - psize/2 > y+(h+bsize)/2) return false;
    else return true;
  }
  
  boolean hit (Char playa)
  {
    if(playa.x + psize/2 < x-w/2) return false;
    else if(playa.y + psize/2 < y-h/2) return false;
    else if(playa.x - psize/2 > x+w/2) return false;
    else if(playa.y - psize/2 > y+h/2) return false;
    else if(barrier == false) return true;
    else return false;
  }
  
}


abstract class Bullet
{
  PVector loc;
  PVector direction;
  
  Bullet (float x, float y)
  {
    loc = new PVector();
    loc.set(x,y);
  }
  
  abstract void draw();
  abstract void go(PVector direction);
  abstract boolean hit(Char playa);
}

class NormalBullet extends Bullet
{
  private float speed=4;
  float dia;
  
  NormalBullet(float x, float y, float dia)
  {
    super(x,y);
    this.dia=dia;
  }
  
  void draw()
  {
    ellipseMode(CENTER);
    noStroke();
    fill(250,100,100);
    ellipse(loc.x,loc.y,dia,dia);
  }
  
  void go(PVector direction)
  {
    loc.set(loc.x+direction.x*speed, loc.y+direction.y*speed);
  }

  
  boolean hit(Char playa)
  {
    if(playa.x + psize/2 < loc.x) return false;
    else if(playa.y + psize/2 < loc.y) return false;
    else if(playa.x - psize/2 > loc.x) return false;
    else if(playa.y - psize/2 > loc.y) return false;
    else return true;
  }
    
}

class GuideBullet extends Bullet
{
  private float easing = 0.005;
  float dia;
  
  GuideBullet(float x, float y, float dia)
  {
    super(x,y);
    this.dia=dia;
  }
  
  void draw()
  {
    ellipseMode(CENTER);
    noStroke();
    fill(100,120,250);
    ellipse(loc.x,loc.y,dia,dia);
  }
  
  void go(PVector direction)
  {
        loc.set(loc.x + (mouseX - loc.x) * easing, loc.y + (mouseY - loc.y) * easing);
  }

  
  boolean hit(Char playa)
  {
    if(playa.x + psize/2 < loc.x) return false;
    else if(playa.y + psize/2 < loc.y) return false;
    else if(playa.x - psize/2 > loc.x) return false;
    else if(playa.y - psize/2 > loc.y) return false;
    else return true;
  }
}


// your code down here
// feel free to crate other .pde files to organize your code
