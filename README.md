# Project 1 template #

This is the template for your project 1 submission. For a full description of the Project, please refer to [this document](https://docs.google.com/document/d/1WhCeZpnAJyuraJI2cAsU8thabzHej7SHa_2b4pQv9Ls/edit?usp=sharing).

### Table of content

* [Source code](Project1_Code/)
* [Demo video](Video/)
* Description of projects and notes in README.md (this file). 
	
	

### Description of the project

Please modify this file to include a description of your project.
You can add images here, as well as links. 
To format this document you will need to use Markdown language. Here a [tutorial](https://bitbucket.org/tutorials/markdowndemo).

Include the following things inside this file:

* briefly describe your project (objectives and what it does)
* describe how to use your project.
* list dependencies, if any (e.g., libraries you used)
* state sources of inspiration/code that you used or if anyone helped you



### 20150622 Changbok Lee 's  project "ABCDE!!"

Hello. This is Changbok Lee.
This README is for my game project named "ABCDE!!"

* briefly describe your project (objectives and what it does)

My project is basically a game.
It's name is "Avoid Bullets & Crash Da Enemy !!"
So like it's name, 
it's objectives are 
 - avoiding bullets as long as player can,
 - crashing enemies as much as you can.
Player controls the round character and takes on the task of 
surviving among the constantly regenerating enemies and crashing them.

* describe how to use your project.

First, in Title Screen, Player can listen a intro music.
Player can start the game by clicking anywhere.

In Ingame Screen, Player should move the mouse.
In game, Player can use a mouse to follow a rounded character.
Also, by bringing this round character to an enemy, player can crash the enemy.

But watch out for bullets!
They can kill the player, 
and when player dead, Player can see Gameover Screen.
(and Player can also listen gameover music)

There are tow types of bullets:
 - Normal Bullets, which going straight in a one direction. (Red ones, small)
 - Guiding Bullets, which approaching the player character. (Blue ones, larger than normal bullet)

There are two types of enemy:
 - Normal Enemy, which regenerates periodically, and shooting normal bullets.
 - Boss Enemy, which regenerats every time 5 normal enemies are spawned, shooting 4 normal bullets at one time, and also shooting guiding bullets toward the player character.

 When the player character hits the bullet, the game is over, 
 and the player can see time and crash history in the Gameover Screen.

 and with the click, player can restart the game.

* list dependencies, if any (e.g., libraries you used)

I used libraries minim to play bgm and sounds in my game,
and I used some design patterns like statemachine and factory.
I also used polymorphism and inheritance to make two types of enemies and two types of bullets.
and I used new ones like millis, frameCount to make some cycles and Loading Screen.

* state sources of inspiration/code that you used or if anyone helped you

I inspired/used my Homeworks I did before in this class.
Especially HW4, which shoot balls to break bricks.
And I also used and checked my before activity in class.
I checked https://processing.org/reference/millis_.html to use millis,
and also https://m.blog.naver.com/PostView.nhn?blogId=cosmosjs&logNo=221026628050&proxyReferer=https:%2F%2Fwww.google.com%2F too, to use library minim.
and 20150822 Youngzu Hwang helped me learning design patterns (a little bit),
but I mainly checked pdf files in class.

Thank you for reading this.
my bitbucket link is this : https://bitbucket.org/imparallel/project1/src/master/ 
Plz enjoy my game "ABCDE !!"